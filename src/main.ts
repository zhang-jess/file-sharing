/*
 * @Author: Jess
 * @Description: 
 * @Date: 2023-11-15 16:28:11
 * @updateInfo: 
 * @LastEditors: Jess
 * @LastEditTime: 2023-11-15 16:52:54
 */
import { createApp } from 'vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import './style.css'
import App from './App.vue'
const app = createApp(App)

app.use(ElementPlus)
app.mount('#app')
