/*
 * @Author: Jess
 * @Description: 文件添加、删除
 * @Date: 2023-11-16 17:23:28
 * @updateInfo: 
 * @LastEditors: Jess
 * @LastEditTime: 2023-11-16 17:27:13
 */
import fs from 'fs';

// 添加文件
const addFile = (fileContent: string, filePath: string) => {
  console.log('%c [ fileContent ]-13', 'font-size:13px; background:pink; color:#bf2c9f;', fileContent, filePath)
  fs.writeFileSync(filePath, JSON.stringify(fileContent));
}

// 删除文件
const deleteFile = (filePath: string) => {
  fs.unlinkSync(filePath);
}

// 导出处理文件操作的函数
export { addFile, deleteFile };